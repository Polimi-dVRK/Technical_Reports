# Technical Reports on the daVinci @ Nearlab

![CC-BY-SA Licence]()https://i.creativecommons.org/l/by-sa/4.0/80x15.png

All the documents in this repository are licenced under CC-BY-SA terms ([Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/))

The documents describe the setup of our daVinci system using the dVRK software. 
